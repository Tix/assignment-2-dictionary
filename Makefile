ASM=nasm
ASM_FLAGS = -f elf64
LD=ld
RM=rm
RM_FLAGS= -rf

main: main.o dict.o lib.o
	$(LD) -o $@ $^


main.o: main.asm lib.inc words.inc colon.inc
	$(ASM) $(ASM_FLAGS) $< -o $@

%.o: %.asm
	$(ASM) $(ASM_FLAGS) -o $@ $<



.PHONY: clean


clean:
	$(RM) $(RM_FLAGS) *.o

