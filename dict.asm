section .text
extern string_equals 
global find_word
%define POINTER_SIZE 8

find_word:
    .loop 
    push rsi
    add rsi, POINTER_SIZE;переходим на следующее слово
    push rdi
    call string_equals 
    pop rdi 
    pop rsi
    cmp rax, 1 ; проверям совпало ли слово
    je .gotcha  
    mov rsi, [rsi] ;переход к следующему элементу
    test rax, rax  ; проверка на несовпдадение 
    je .not_found 
    jmp .loop 

    .gotcha:
        mov rax, rsi ;кладем адрес начала вхождение
        ret 
    .not_found
        xor rax, rax 
        ret 
