
%include "lib.inc"
%include "word.inc" 
%define BUF_SIZE 255 ; устанавливаем размер буфера 
%define STDERR 2
%define SYS_CALL 1
; служебные сообщения
section .rodata
    buf_overflow_error_msg: db "buffer overflow!", 0 
    unfound_key_error_msg: db "key was not found", 0
    welcome_msg: db "Hi! Enter the key word ", 0 

; определяем буфер 

section .bss
    buffer: resb BUF_SIZE ;задаем размер буферу



global _start 
extern find_word
section .text
_start: 

    mov rdi, welcome_msg
    call print_string ; выводим привественное сообщение 

    mov rdi, buffer 
    mov rsi, BUF_SIZE
    call read_word ; читаем слово, передавая адрес буфера и его размер 
    test rax, rax 
    je .buf_overflow_error ;отработка случая переполнения 
    mov rdi, buffer 
    mov rsi, current 
    call find_word ; ищем совпдание в списке 
    test rax, rax 
    je .unfound_key_error ;если не нашли 
    add rax, 8
    mov rdi, rax ; переход на адрес ключа 
    push rdi 
    call string_length
    pop rdi 
    inc rdi 
    add rdi, rax 
    call print_string  ; выводим результат, заранее заполнив все регистры 
    call exit 


    .buf_overflow_error:
        mov rsi, buf_overflow_error_msg
        mov rdx, 17
        jmp .error_printer 
    .unfound_key_error:
        mov rsi, unfound_key_error_msg
        mov rdx, 17 
        jmp .error_printer
    .error_printer:
        mov rax, SYS_CALL                                              
        mov rdi, STDERR                                            
        syscall 
        jmp exit

